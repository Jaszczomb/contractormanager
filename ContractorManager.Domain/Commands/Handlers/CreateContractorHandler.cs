﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ContractorManager.Domain.Aggregates;
using OpenCqrs.Domain;

namespace ContractorManager.Domain.Commands.Handlers
{
    public class CreateContractorHandler : IDomainCommandHandlerAsync<CreateContractor>
    {
        public async Task<IEnumerable<IDomainEvent>> HandleAsync(CreateContractor command)
        {
            await Task.CompletedTask;

            var product = new Contractor(command.AggregateRootId, command.Title);

            return product.Events;
        }
    }
}
