﻿using OpenCqrs.Domain;

namespace ContractorManager.Domain.Commands
{
    public class CreateContractor : DomainCommand
    {
        public string Title { get; set; }
    }
}
