﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ContractorManager.Domain.Queries;
using OpenCqrs.Queries;

namespace ContractorManager.Domain.QueryHandlers
{
    public class GetAllContractorsHandlerAsync : IQueryHandlerAsync<GetAllContractors, List<ContractorViewModel>>
    {
        public async Task<List<ContractorViewModel>> RetrieveAsync(GetAllContractors query)
        {
            await Task.CompletedTask;

            var model = FakeReadDatabase.Contractors;
            return model;
        }
    }
}
