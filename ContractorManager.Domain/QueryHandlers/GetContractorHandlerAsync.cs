﻿using System.Threading.Tasks;
using ContractorManager.Domain.Queries;
using OpenCqrs.Queries;

namespace ContractorManager.Domain.QueryHandlers
{
    public class GetContractorHandlerAsync : IQueryHandlerAsync<GetContractor, ContractorViewModel>
    {
        public async Task<ContractorViewModel> RetrieveAsync(GetContractor query)
        {
            await Task.CompletedTask;

            var model = FakeReadDatabase.Contractors.Find(x => x.Id == query.Id);
            return model;
        }
    }
}
