﻿using System;
using OpenCqrs.Queries;

namespace ContractorManager.Domain.Queries
{
    public class GetContractor : IQuery
    {
        public Guid Id { get; set; }
    }
}
