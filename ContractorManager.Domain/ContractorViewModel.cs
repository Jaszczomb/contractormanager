﻿using System;

namespace ContractorManager.Domain
{
    public class ContractorViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
    }
}
