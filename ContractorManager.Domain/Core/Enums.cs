﻿// =============================
// Email: info@ebenmonney.com
// www.ebenmonney.com/templates
// =============================

namespace ContractorManager.Domain.Core
{
    public enum Gender
    {
        None,
        Female,
        Male
    }
}
