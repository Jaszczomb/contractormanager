﻿using OpenCqrs.Domain;

namespace ContractorManager.Domain.Events
{
    public class ContractorTitleUpdated : DomainEvent
    {
        public string Title { get; set; }
    }
}
