﻿using ContractorManager.Domain.Aggregates;
using OpenCqrs.Domain;

namespace ContractorManager.Domain.Events
{
    public class ContractorCreated : DomainEvent
    {
        public string Title { get; set; }
        public ContractorStatus Status { get; set; }
    }
}
