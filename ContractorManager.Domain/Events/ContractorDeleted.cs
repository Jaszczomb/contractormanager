﻿using OpenCqrs.Domain;

namespace ContractorManager.Domain.Events
{
    public class ContractorDeleted : DomainEvent
    {
    }
}
