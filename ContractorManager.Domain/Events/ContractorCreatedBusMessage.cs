﻿using System;
using ContractorManager.Domain.Aggregates;
using OpenCqrs.Bus;
using OpenCqrs.Domain;

namespace ContractorManager.Domain.Events
{
    public class ContractorCreatedBusMessage : DomainEvent, IBusTopicMessage
    {
        public string Title { get; set; }
        public ContractorStatus Status { get; set; }

        public DateTime? ScheduledEnqueueTimeUtc { get; set; }
        public string TopicName { get; set; } = "product-created";
    }
}
