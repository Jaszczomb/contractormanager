﻿using System.Collections.Generic;

namespace ContractorManager.Domain
{
    public static class FakeReadDatabase
    {
        public static List<ContractorViewModel> Contractors = new List<ContractorViewModel>();
    }
}
