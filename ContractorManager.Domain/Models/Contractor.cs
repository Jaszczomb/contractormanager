﻿// =============================
// Email: info@ebenmonney.com
// www.ebenmonney.com/templates
// =============================

using System;
using System.Collections.Generic;

namespace ContractorManager.Domain.Models
{
    public class Contractor : AuditableEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public decimal BuyingPrice { get; set; }
        public decimal SellingPrice { get; set; }
        public int UnitsInStock { get; set; }
        public bool IsActive { get; set; }
        public bool IsDiscontinued { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }


        public int? ParentId { get; set; }
        public Contractor Parent { get; set; }

        public int ProductCategoryId { get; set; }
        public ContractorCategory ContractorCategory { get; set; }

        public ICollection<Contractor> Children { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
