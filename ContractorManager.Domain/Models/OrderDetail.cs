﻿// =============================
// Email: info@ebenmonney.com
// www.ebenmonney.com/templates
// =============================

namespace ContractorManager.Domain.Models
{
    public class OrderDetail : AuditableEntity
    {
        public int Id { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public decimal Discount { get; set; }


        public int ContractorId { get; set; }
        public Contractor Contractor { get; set; }

        public int OrderId { get; set; }
        public Order Order { get; set; }
    }
}
