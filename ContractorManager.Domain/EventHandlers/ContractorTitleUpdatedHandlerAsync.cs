﻿using System.Threading.Tasks;
using ContractorManager.Domain.Events;
using OpenCqrs.Events;

namespace ContractorManager.Domain.EventHandlers
{
    public class ContractorTitleUpdatedHandlerAsync : IEventHandlerAsync<ContractorTitleUpdated>
    {
        public async Task HandleAsync(ContractorTitleUpdated @event)
        {
            await Task.CompletedTask;

            var model = FakeReadDatabase.Contractors.Find(x => x.Id == @event.AggregateRootId);
            model.Title = @event.Title;
        }
    }
}
