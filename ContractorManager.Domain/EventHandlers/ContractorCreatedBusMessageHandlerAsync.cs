﻿using System.Threading.Tasks;
using ContractorManager.Domain.Events;
using OpenCqrs.Events;

namespace ContractorManager.Domain.EventHandlers
{
    public class ContractorCreatedBusMessageHandlerAsync : IEventHandlerAsync<ContractorCreatedBusMessage>
    {
        public async Task HandleAsync(ContractorCreatedBusMessage @event)
        {
            await Task.CompletedTask;

            var model = new ContractorViewModel
            {
                Id = @event.AggregateRootId,
                Title = @event.Title
            };

            FakeReadDatabase.Contractors.Add(model);
        }
    }
}
