﻿using System.Threading.Tasks;
using ContractorManager.Domain.Events;
using OpenCqrs.Events;

namespace ContractorManager.Domain.EventHandlers
{
    public class ContractorCreatedHandlerAsync : IEventHandlerAsync<ContractorCreated>
    {
        public async Task HandleAsync(ContractorCreated @event)
        {
            await Task.CompletedTask;

            var model = new ContractorViewModel
            {
                Id = @event.AggregateRootId,
                Title = @event.Title
            };

            FakeReadDatabase.Contractors.Add(model);
        }
    }
}
