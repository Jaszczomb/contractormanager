﻿using System;
using ContractorManager.Domain.Events;
using OpenCqrs.Domain;

namespace ContractorManager.Domain.Aggregates
{
    public class Contractor : AggregateRoot
    {
        public string Title { get; private set; }
        public ContractorStatus Status { get; private set; }

        public Contractor()
        {            
        }

        public Contractor(Guid id, string title) : base(id)
        {
            if (string.IsNullOrEmpty(title))
                throw new ApplicationException("Contractor title is required.");

            // If you want the event to be dispatched to the service bus,
            // use ContractorCreatedBusMessage instead of ContractorCreated.
            // Remember to update the connection string in the ServiceBusConfiguration
            // section in the appsettings.json file.
            AddEvent(new ContractorCreated
            {
                AggregateRootId = Id,
                Title = title,
                Status = ContractorStatus.Draft
            });
        }

        public void UpdateTitle(string title)
        {
            if (string.IsNullOrEmpty(title))
                throw new ApplicationException("Contractor title is required.");

            AddEvent(new ContractorTitleUpdated
            {
                AggregateRootId = Id,
                Title = title
            });
        }

        public void Publish()
        {
            AddEvent(new ContractorPublished
            {
                AggregateRootId = Id
            });
        }

        public void Delete()
        {
            AddEvent(new ContractorDeleted
            {
                AggregateRootId = Id
            });
        }

        private void Apply(ContractorCreated @event)
        {
            Id = @event.AggregateRootId;
            Title = @event.Title;
            Status = @event.Status;
        }

        private void Apply(ContractorCreatedBusMessage @event)
        {
            Id = @event.AggregateRootId;
            Title = @event.Title;
            Status = @event.Status;
        }

        private void Apply(ContractorTitleUpdated @event)
        {
            Title = @event.Title;
        }

        private void Apply(ContractorPublished @event)
        {
            Status = ContractorStatus.Published;
        }

        private void Apply(ContractorDeleted @event)
        {
            Status = ContractorStatus.Deleted;
        }
    }
}
