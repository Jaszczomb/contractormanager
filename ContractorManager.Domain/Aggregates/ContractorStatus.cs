﻿namespace ContractorManager.Domain.Aggregates
{
    public enum ContractorStatus
    {
        Draft = 0,
        Published = 1,
        Deleted = 2
    }
}
