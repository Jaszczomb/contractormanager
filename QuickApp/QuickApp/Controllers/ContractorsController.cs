﻿// =============================
// Email: info@ebenmonney.com
// www.ebenmonney.com/templates
// =============================

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ContractorManager.App.Helpers;
using ContractorManager.App.ViewModels;
using ContractorManager.Domain.Queries;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OpenCqrs;

namespace ContractorManager.App.Controllers
{
    [Route("api/[controller]")]
    public class ContractorsController : Controller
    {
        private readonly IDispatcher _dispatcher;
        readonly ILogger _logger;
        readonly IEmailSender _emailSender;


        public ContractorsController(IDispatcher dispatcher, ILogger<ContractorsController> logger, IEmailSender emailSender)
        {
            _dispatcher = dispatcher;
            _logger = logger;
            _emailSender = emailSender;
        }



        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            var allContractors = _dispatcher.GetResult<GetAllContractors, List<ContractorViewModel>>(new GetAllContractors());
            return Ok(Mapper.Map<IEnumerable<ContractorViewModel>>(allContractors));
        }


        [HttpGet("throw")]
        public IEnumerable<ContractorViewModel> Throw()
        {
            throw new InvalidOperationException("This is a test exception: " + DateTime.Now);
        }



        [HttpGet("email")]
        public async Task<string> Email()
        {
            string recepientName = "QickApp Tester"; //         <===== Put the recepient's name here
            string recepientEmail = "test@ebenmonney.com"; //   <===== Put the recepient's email here

            string message = EmailTemplates.GetTestEmail(recepientName, DateTime.UtcNow);

            (bool success, string errorMsg) = await _emailSender.SendEmailAsync(recepientName, recepientEmail, "Test Email from QuickApp", message);

            if (success)
                return "Success";

            return "Error: " + errorMsg;
        }



        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value: " + id;
        }



        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }



        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }



        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
