// =============================
// Email: info@ebenmonney.com
// www.ebenmonney.com/templates
// =============================

import { Component } from '@angular/core';
import { fadeInOut } from '../../services/animations';


@Component({
  selector: 'contractors',
    templateUrl: './contractors.component.html',
    styleUrls: ['./contractors.component.scss'],
    animations: [fadeInOut]
})
export class ContractorsComponent {

}
