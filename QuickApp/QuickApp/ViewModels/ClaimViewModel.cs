﻿// =============================
// Email: info@ebenmonney.com
// www.ebenmonney.com/templates
// =============================

namespace ContractorManager.App.ViewModels
{
    public class ClaimViewModel
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }
}
